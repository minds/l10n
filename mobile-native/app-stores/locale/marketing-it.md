## Descrizione breve

Riprendi il controllo dei tuoi social media

## Full description

Libera la tua mente e vieni pagato per creare contenuti, stimolare il coinvolgimento e portare i tuoi amici. Siamo una piattaforma di social network per conversazioni aperte e per riunire le persone secondo i principi di libertà, trasparenza e privacy. Sei tu che controlli la tua esperienza.

We make it simple to discover and capture your own original videos, blogs and photos with easy-to-use tools to watch and share your daily thoughts and experiences.

■ Discover endless videos, photos and blogs from your favorite creators.

■ Confrontati con persone da tutto il mondo che apprezzano la libertà di parola e hanno opinioni forti. Aiuta a cambiare idea o a considerare il punto di vista di qualcun altro.

Guadagna denaro in più valute per creare contenuti originali e far crescere il tuo pubblico. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Chat with your friends and contacts privately with encrypted messenger. Sii anonimo se lo desideri.

■ Vendi contenuti digitali e abbonamenti premium ai tuoi fan.

■ Boost and promote your content for more exposure. Guadagna o acquista token per più visualizzazioni dei tuoi contenuti. Nessun algoritmo o sorveglianza. Un token ti dà 1.000 visualizzazioni extra. 100% chronological and organic reach in your newsfeed.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Per supporto, domande o ulteriori informazioni, visita: https://www.minds.com/help

Codice open source: https://developers.minds.com

Contattaci su info@minds.com
