## Korte beschrijving

Neem de controle over uw sociale media terug

## Volledige beschrijving

Maak je geest vrij en krijg betaald voor het maken van inhoud, het stimuleren van betrokkenheid en het verwijzen van vrienden. We zijn een sociaal netwerkplatform om open gesprekken te voeren en mensen samen te brengen onder de principes van vrijheid, transparantie en privacy. Jij bepaalt je ervaring.

We maken het eenvoudig om uw eigen originele video's, blogs en foto's te ontdekken en vast te leggen met gebruiksvriendelijke tools om uw dagelijkse gedachten en ervaringen te bekijken en te delen.

■ Ontdek eindeloze video's, foto's en blogs van je favoriete makers.

■ Kom in contact met mensen van over de hele wereld die vrijheid van meningsuiting waarderen en een uitgesproken mening hebben. Verander van gedachten of overweeg het standpunt van iemand anders.

■ Verdien geld in meerdere valuta's om populaire originele inhoud te maken en uw publiek te vergroten. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Chat privé met uw vrienden en contacten met gecodeerde messenger. Wees anoniem als je dat wilt.

■ Sell digital content and premium subscriptions to your fans.

■ Verhoog en promoot uw inhoud voor meer zichtbaarheid. Verdien of koop tokens voor meer weergaven van uw inhoud. Geen algoritmen of toezicht. One token gets you 1,000 extra views. 100% chronological and organic reach in your newsfeed.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Voor ondersteuning, vragen of meer informatie, bezoek: https://www.minds.com/help

Open broncode: https://developers.minds.com

Neem contact met ons op via info@minds.com
