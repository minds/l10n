## Stručný popis

Získejte zpět kontrolu nad svojí sociální sítí

## Celý popis

Osvoboďte svoji mysl a dostávejte zaplaceno za vytváření obsahu, zvyšování aktivity a doporučování přátel. Jsme sociální síť, která podporuje otevřené konverzace a spojuje lidi pomocí principů svobody, transparentnosti a soukromí. Vy kontrolujete svůj zážitek.

Usnadňujeme objevování a vytváření vašich vlastních originálních videí, blogů a fotek pomocí jednoduchých nástrojů pro sledování a sdílení každodenních myšlenek a zážitků.

■ Objevujte nekonečná videa, fotky a blogy od svých oblíbených tvůrců.

■ Komunikujte s lidmi z celého světa, kteří si cení svobody projevu a mají silné názory. Pomozte změnit něčí názor nebo se zamyslete nad úhlem pohledu někoho jiného.

■ Vydělávejte peníze ve více měnách za vytváření populárního originálního obsahu a růst vašeho publika. Každý den měříme váš příspěvek ve vztahu ke komunitě a odměňujeme vás pomocí tokenů díky blockchainové technologi pro maximální transparentnost a kontrolu.

■ Chatujte se svými přáteli a kontakty soukromě pomocí šifrovaného messengeru. Pokud chcete, buďte anonymní.

■ Prodávejte digitální obsah a prémiové předplatné vašim fanouškům.

■ Boostujte a propagujte svůj obsah pro větší expozici. Vydělávejte nebo kupujte tokeny pro více zhlédnutí vašeho obsahu. Žádné algoritmy ani sledování. Jeden token vám dá 1000 dalších zhlédnutí. 100% chronologický a organický dosah ve vašem kanálu novinek.

■ Funkce: Jednoduchá tvorba mobilních videí, kanál novinek, peněženka (Bitcoin, Ether, Minds Tokeny, USD), videochat, skupiny, blogy, video, fotografie, vyhledávání, objevování, statusy, analýzy, porota, šifrovaný chat a více...

Pro podporu, otázky nebo více informací navštivte prosím: https://www.minds.com/help

Otevřený zdrojový kód: https://developers.minds.com

Kontaktujte nás na info@minds.com
