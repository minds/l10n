## Descrição breve

Assuma o controle das suas redes sociais

## Descrição completa

Liberta a tua mente e sê pago para criar conteúdo, conduzir envolvimento e referir amigos. Somos uma plataforma de redes sociais onde pode ter conversas abertas e reunir pessoas sob os princípios de liberdade, transparência e privacidade. Tu controlas a tua experiência.

Tornamos simples a descoberta e captura dos teus vídeos originais, blogues e fotos com ferramentas fáceis de utilizar para veres e partilhares os teus pensamentos e experiências do dia-a-dia.

■ Descobre vídeos, fotos e blogues infinitos dos teus criadores favoritos.

■ Comunica com pessoas de todo o mundo que valorizam a liberdade de expressão e que têm opiniões convictas. Ajuda a mudar mentalidades ou considera o ponto de vista de outra pessoa.

■ Ganha dinheiro em várias moedas por criares conteúdo popular e original e aumentares o teu público. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Conversa com os teus amigos e contactos em privado com mensagens encriptadas. Sê anónimo se quiseres.

■ Vende conteúdo digital e subscrições premium aos teus fãs.

■ Aumenta e promove o teu conteúdo para mais exposição. Ganha ou compra tokens para teres mais visualizações no teu conteúdo. Sem algoritmos ou vigilância. Um token dá-te 1000 visualizações extra. 100% cronológico e orgânico no teu feed de notícias.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Para suporte, questões ou mais informações, por favor visita: https://www.minds.com/help

Código de fonte aberto: https://developers.minds.com

Contacte-nos em info@minds.com
