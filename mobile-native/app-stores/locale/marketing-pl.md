## Short description

Odzyskaj kontrolę nad swoimi mediami społecznościowymi

## Full description

Uwolnij swój umysł i zarabiaj publikując treści, kreując zainteresowanie i polecając znajomym. Jesteśmy platformą społecznościową umożliwiająca wolną wymianę poglądów oraz zbliżającą ludzi w duchu takich wartości jak wolność, transparentność i prywatność. Ty kontrolujesz co publikujesz.

Uprościliśmy odkrywanie i tworzenia własnych oryginalnych filmów, blogów oraz zdjęć, dzięki łatwym w użyciu narzędziom do przeglądania i udostępniania twoich codziennych przemyśleń i przeżyć.

■ Odkrywaj niezliczoną ilość filmów, zdjęć i blogów swoich ulubionych twórców.

■ Współpracuj z ludźmi z całego świata, którzy cenią wolność słowa i mają odważne poglądy. Pomóż innym zmienić poglądy i rozważ ich punkt widzenia.

■ Zarabiaj w wielu walutach za tworzenie popularnych i oryginalnych treści oraz powiększanie grona swoich odbiorców. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Prowadź chat ze znajomymi oraz osobami z listy kontaktów korzystając z szyfrowanego komunikatora. Bądź anonimowy, jeżeli chcesz.

■ Sprzedawaj swoim fanom cyfrowe materiały oraz subskrypcje premium.

■ Pobudzaj zainteresowanie i promuj swoje publikacje dla lepszej ekspozycji w sieci. Zarabiaj i kupuj tokeny by zwiększyć ilość wyświetleń swoich treści. Nie stosujemy algorytmów ani śledzenia. Jeden token daje Ci 1000 dodatkowych wyświetleń. 100% chronologiczny i organiczny zasięg w twoim kanale informacyjnym.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Wsparcie techniczne, odpowiedzi na pytania oraz więcej informacji można znaleźć na stronie: https://www.minds.com/help

Kod źródłowy: https://developers.minds.com

Skontaktuj się z nami pod adresem: info@minds.com
