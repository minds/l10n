## Description courte

Reprenez le contrôle de vos réseaux sociaux

## Description complète

Libérez votre esprit et soyez payé pour créer du contenu, susciter l'engagement et parrainer des amis. Nous sommes une plateforme de réseau social pour avoir des conversations ouvertes et rassembler les gens selon les principes de liberté, de transparence et de confidentialité. Vous contrôlez votre expérience.

Nous simplifions la découverte et la création de vidéos, blogs et photos avec des outils faciles à utiliser. Capturez et partagez vos pensées et expériences quotidiennes.

■ Découvrez des tonnes de vidéos, photos et blogs produites par vos créateurs favoris.

■ Discutez avec des personnes venant du monde entier, qui valorisent la liberté d'expression et ont des opinions complexes à partager. Aidez quelqu'un à changer son point de vue, ou découvrez celui d'un autre.

■ Gagnez de l'argent dans de multiples devises en créant des contenus originaux populaires et en faisant croître votre public. Chaque jour, nous quantifions votre contribution par rapport à la communauté et vous récompensons avec des jetons qui utilisent la technologie blockchain pour un maximum de contrôle et de transparence.

■ Discutez avec vos amis et contacts grâce à une messagerie chiffrée. Restez anonyme si vous le désirez.

■ Vendez du contenu digital et des abonnements premium à vos fans.

■ Boostez et promouvez votre contenu pour bénéficier d'une meilleure exposition. Gagnez ou achetez des jetons pour obtenir plus de vues. Pas d'algorithmes ni de surveillance. Un jeton vous donne 1000 vues supplémentaires. Votre fil d'actualité est 100% chronologique et organique.

■ Inclut : création facile de vidéos sur mobile, fil d'actualité, portefeuille (Bitcoin, Ether, jetons Minds, dollars), conversation vidéo, groupes, blogs, vidéos, photos, recherche, fil de découvertes, statuts, analytises, jurys, discussion chiffrée, et plus encore...

Si vous avez besoin d'aide ou de plus d'informations, consultez : https://www.minds.com/help

Code open source : https://developers.minds.com

Contactez-nous à info@minds.com
