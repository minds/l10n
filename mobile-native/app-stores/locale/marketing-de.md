## Kurze Beschreibung

Übernimm die Kontrolle deiner sozialen Medien zurück

## Vollständige Beschreibung

Befreie deinen Verstand und werde für das erstellen von Inhalten, Engagement treiben und Freunde empfehlen bezahlt. Wir sind ein soziales Netzwerk, dass offene Konversationen hat und Menschen unter den Prinzipien der Freiheit, Transparenz und Privatsphäre zusammenbringt. Du hast die Kontrolle über deine Erfahrungen.

Wir erleichtern es deine eigenen Videos, Blogs und Photos mit leicht zu benutzenden Werkzeugen zu entdecken und zu erfassen, zu sehen und deine täglichen Gedanken und Erfahrungen zu teilen.

■ Entdecke unendliche Videos, Photos und Blogs deiner Lieblingsurheber.

■ Vernetze dich mit Menschen aus der ganzen Welt, die Wert auf freie Meinungsäußerung legen und starke Ansichten haben. Helfe Meinungen zu ändern oder ziehe die Meinungen anderer in Betracht.

■ Verdiene für das Erstellen von beliebten Inhalten und das Wachsen deines Publikums Geld in verschiedenen Währungen. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Chatte privat mit deinen Freunden und Kontakten mit dem verschlüsselten Messenger. Sei anonym, wenn du das möchtest.

■ Verkaufe deinen Fans digitale Inhalte und premium Abonnements.

■ Bewerbe deine Inhalte, um die Publicity zu erhöhen. Earn or buy tokens for more views on your content. Keine Algorithmen oder Überwachung. One token gets you 1,000 extra views. 100% chronological and organic reach in your newsfeed.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Für Unterstützung, Fragen oder mehr Informationen besuche bitte: https://www.minds.com/help

Offener Quelltext: https://developers.minds.com

Kontaktiere uns: info@minds.com
