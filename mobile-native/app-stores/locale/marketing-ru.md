## Краткое описание

Take back control of your social media

## Полное описание

Free your mind and get paid for creating content, driving engagement and referring friends. Мы являемся социальной платформой для проведения открытых бесед и объединения людей в соответствии с принципами свободы, прозрачности и конфиденциальности. Вы контролируете свой опыт.

Мы упрощаем процессы нахождения и запечатления своих собственных оригинальных видео, блогов и фотографий с помощью простых в использовании инструментов для просмотра и обмена своими ежедневными мыслями и опытом.

■ Откройте для себя бесконечные видео, фотографии и блоги от ваших любимых создателей контента.

■ Взаимодействуйте с людьми со всего мира, ценящими свободу слова и имеющими сильные мнения. Помогите изменить мнения или рассмотрите чужую точку зрения.

■ Зарабатывайте деньги в нескольких валютах за создание популярного оригинального контента и увеличение своей аудитории. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Chat with your friends and contacts privately with encrypted messenger. Будьте анонимны, если хотите.

■ Продавайте цифровой контент и премиальные подписки вашим фанатам.

■ Ускоряйте и продвигайте ваш контент для большего воздействия. Зарабатывайте или покупайте жетоны для увеличения просмотров вашего контента. Никаких алгоритмов или слежения. Один жетон дает вам 1000 дополнительных просмотров. 100% хронологический и органический охват в вашей новостной ленте.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Для поддержки, вопросов или более подробной информации пожалуйста посетите https://www.minds.com/help

Открытый исходный код: https://developers.minds.com

Свяжитесь с нами по адресу info@minds.com
