## Kort beskrivelse

Ta tilbake kontrollen over dine sosiale medier

## Fullstendig beskrivelse

Befri tankene dine og bli betalt for å skape innhold, drive engasjement og invitere venner. Vi er en sosial nettverksplattform for åpne samtaler og det å bringe mennesker sammen under prinsippene frihet, åpenhet og personvern. Du styrer din opplevelse.

Vi gjør det enkelt å oppdage og lage dine egne originale videoer, blogger og bilder med enkle verktøy for å se og dele dine daglige tanker og opplevelser.

■ Oppdag en endeløs strøm av videoer, bilder og blogger fra dine favorittskapere.

■ Knytt kontakt med mennesker verden rundt, som verdsetter ytringsfrihet og egne meninger. Hjelp til med å endre tanker og idèer, eller vurder andre sine synspunkter.

■ Tjen penger i flere valutaer ved å skape populært originalinnhold og øke følgerskaren din. Hver dag måler vi ditt bidrag relativt til Minds-samfunnets og belønner deg deretter med tokener ved hjelp av blokkjedeteknologi for maksimal åpenhet og kontroll.

■ Chat med vennene og kontaktene dine privat med kryptert meldinger. Vær anonym om du ønsker det.

■ Selg digitalt innhold og premiumabonnementer til dine fans.

■ Boost og promoter ditt innhold for økt eksponering. Tjen eller kjøp tokens for flere visninger av innholdet ditt. Ingen algoritmer eller overvåking. En token gir deg 1000 ekstra visninger. 100% kronologisk og organisk nyhetsstrøm.

■ Funksjoner: Enkel videooppretting, Nyhetsstrøm, Lommebok for kryptovaluta (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Grupper, Blogger, Video, Bilder, Søk, Utforsk, Statuser, Analyse, Juryer, Kryptert chat, med mer...

For støtte, spørsmål eller mer informasjon, besøk: https://www.minds.com/help

Åpen kildekode: https://developers.minds.com

Kontakt oss på info@minds.com
