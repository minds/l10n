## Short description

Take back control of your social media

## Full description

Free your mind and get paid for creating content, driving engagement and referring friends. We are a social networking platform to have open conversations and bring people together under the principles of freedom, transparency and privacy. You control your experience.

We make it simple to discover and capture your own original videos, blogs and photos with easy-to-use tools to watch and share your daily thoughts and experiences.

■ Discover endless videos, photos and blogs from your favorite creators.

■ Engage with people from all around the world who value free speech and have strong opinions. Help change minds or consider someone else’s point of view.

■ Earn money in multiple currencies for creating popular original content and growing your audience. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Chat with your friends and contacts privately with encrypted messenger. Be anonymous if you wish.

■ Sell digital content and premium subscriptions to your fans.

✓ Aumenta y promociona tu contenido para más exposición. Gana o compra fichas para obtener más vistas sobre tu contenido. Sin algoritmos ni vigilancia. Una ficha te da 1.000 vistas adicionales. 100% chronological and organic reach in your newsfeed.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

For support, questions, or more information, please visit: https://www.minds.com/help

Open source code: https://developers.minds.com

Contáctenos en info@minds.com
