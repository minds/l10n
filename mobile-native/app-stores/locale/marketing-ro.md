## Descriere pe scurt

Preia din nou controlul asupra rețelelor sociale

## Descriere completă

Free your mind and get paid for creating content, driving engagement and referring friends. Suntem o platformă de socializare pentru a avea conversații deschise și a reuni oamenii în conformitate cu principiile libertății, transparenței și vieții private. Tu controlezi experiența ta.

We make it simple to discover and capture your own original videos, blogs and photos with easy-to-use tools to watch and share your daily thoughts and experiences.

■ Descoperă videoclipuri nesfârșite, fotografii și bloguri de la creatorii tăi preferați.

■ Interacționează cu oameni din întreaga lume care apreciază libertatea de exprimare și au opinii puternice. Ajută la schimbarea mentalității sau ia în considerare punctul de vedere al altcuiva.

■ Câștigă bani în mai multe valute pentru crearea conținutului original popular și pentru creșterea audienței. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Discuta cu prietenii tăi și persoanele de contact private cu mesagerul criptat. Fii anonim dacă dorești.

■ Vinde conținut digital și abonamente premium fanilor tăi.

■ Îmbunătățește și promovează conținutul tau pentru mai multă expunere. Câștigă sau cumpără jetoane pentru mai multe vizualizări despre conținutul tău. Fără algoritmi sau supraveghere. Un token îți aduce 1000 de vizualizări în plus. Acoperire 100% cronologică și organică în fluxul tău de știri.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Pentru suport, întrebări sau mai multe informații, vizitează: https://www.minds.com/help

Cod sursă la liber: https://developers.minds.com

Contactează-ne la info@minds.com
