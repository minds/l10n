## Korte beschrijving

Neem terug controle over je sociale media

## Volledige beschrijving

Geef je verstand de vrijheid en word financieel beloond voor creatieve inhoud, betrokkenheid te stimuleren en door vrienden aan te brengen. Wij zijn een sociaal netwerk platform voor vrije gesprekken, en brengen mensen samen volgens de principes van vrijheid, doorzichtigheid en geheimhouding. U controleert uw ervaring.

We maken het gemakkelijk om je eigen digitale video’s, blogs en foto’s te ontdekken en vast te leggen met handige tools om je dagelijkse gedachten en ervaringen te bekijken en te delen.

Ontdek een onuitputtelijk aantal video’s en blogs van je favoriete auteurs.

Verbind met mensen uit de gehele wereld die vrije meningen en sterke opinies belangrijk vinden. Help anderen om van gedachten te veranderen of neem een andere mening in overweging.

Verdien geld in verschillende valuta door populaire en originele stukken te ontwerpen en hierdoor je publiek te laten groeien. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Chat with your friends and contacts privately with encrypted messenger. Be anonymous if you wish.

■ Sell digital content and premium subscriptions to your fans.

■ Boost and promote your content for more exposure. Earn or buy tokens for more views on your content. No algorithms or surveillance. One token gets you 1,000 extra views. 100% chronological and organic reach in your newsfeed.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

For support, questions, or more information, please visit: https://www.minds.com/help

Open source code: https://developers.minds.com

Contact us at info@minds.com
