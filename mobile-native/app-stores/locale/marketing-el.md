## Σύντομη περιγραφή

Πάρτε ξανά τον έλεγχο των κοινωνικών μέσων σας

## Πλήρης περιγραφή

Ελευθερώστε το μυαλό σας και πληρώστε για τη δημιουργία περιεχομένου, την προώθηση αφοσίωσης και την παραπομπή φίλων. Είμαστε μια πλατφόρμα κοινωνικής δικτύωσης που έχει ανοιχτές συνομιλίες και φέρνει κοντά τους ανθρώπους στις αρχές της ελευθερίας, της διαφάνειας και της ιδιωτικής ζωής. Εσείς ελέγχετε την εμπειρία σας.

We make it simple to discover and capture your own original videos, blogs and photos with easy-to-use tools to watch and share your daily thoughts and experiences.

■ Discover endless videos, photos and blogs from your favorite creators.

■ Αλληλεπιδράστε με άτομα από όλο τον κόσμο που εκτιμούν την ελεύθερη ομιλία και έχουν ισχυρές απόψεις. Βοηθήστε να αλλάξετε γνώμη ή να σκεφτείτε την άποψη κάποιου άλλου. Βοηθήστε να αλλάξετε γνώμη ή να σκεφτείτε την άποψη κάποιου άλλου.

■ Earn money in multiple currencies for creating popular original content and growing your audience. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

Συνομιλήστε με τους φίλους και τις επαφές σας ιδιωτικά με κρυπτογραφημένο messenger. Να είστε ανώνυμοι αν θέλετε.

Πουλήστε ψηφιακό περιεχόμενο και premium συνδρομές στους θαυμαστές σας.

Ενισχύστε και προωθήστε το περιεχόμενό σας για περισσότερη έκθεση. Κερδίστε ή αγοράστε μάρκες για περισσότερες προβολές στο περιεχόμενό σας. Χωρίς αλγόριθμους ή επιτήρηση. Ένα διακριτικό σας δίνει 1.000 επιπλέον προβολές. 100% chronological and organic reach in your newsfeed.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Για υποστήριξη, ερωτήσεις ή περισσότερες πληροφορίες, επισκεφθείτε τη διεύθυνση: https://www.minds.com/help

Επικοινωνήστε μαζί μας στο info@minds.com

Contact us at info@minds.com
