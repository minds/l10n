## Kısa açıklama

Take back control of your social media

## Ayrıntılı Açıklama

Zihninizi özgürleştirin ve yarattığınız içerik, oluşturduğunuz etkileşim ve arkadaşlarınıza yaptığınız yönlendirmeler için para kazanın. Zihninizi özgürleştirin ve yarattığınız içerik, oluşturduğunuz etkileşim ve arkadaşlarınıza yaptığınız yönlendirmeler için para kazanın. Biz özgürlük, saydamlık ve gizlilik ilkeleri altında açık iletişimi sağlayan ve insanları birleştiren bir sosyal ağ platformuyuz. Deneyiminiz sizin kontrolünüzde. Deneyiminiz sizin kontrolünüzde.

We make it simple to discover and capture your own original videos, blogs and photos with easy-to-use tools to watch and share your daily thoughts and experiences.

■ Discover endless videos, photos and blogs from your favorite creators.

■ Dünyanın her yerinden özgür düşünceye değer veren ve güçlü fikirleri olan insanlar ile etkileşime geçin. Zihinleri değiştirmeye yardımcı olun veya bir başkasının açısından bakın. Zihinleri değiştirmeye yardımcı olun veya bir başkasının açısından bakın.

■ Popüler orijinal içerikler oluşturarak ve izleyici kitlenizi genişleterek birden fazla birimde para kazanın. Each day we measure your contribution relative to the community and reward you with tokens using blockchain technology for maximum transparency and control.

■ Chat with your friends and contacts privately with encrypted messenger. İstediğinizde anonim olun.

■ Hayranlarınıza dijital içerik ve ayrıcalıklı üyelik satın alma imkanı sunun.

■ Yükseltin ve içeriğinizi tanıtarak daha görünür hale getirin. Earn or buy tokens for more views on your content. Algoritma veya gözetim yok. Bir token size 1000 ekstra görünüm kazandırır. 100% chronological and organic reach in your newsfeed.

■ Features: Easy mobile video creation, Newsfeed, Wallet (Bitcoin, Ether, Minds Tokens, USD), Video Chat, Groups, Blogs, Video, Photos, Search, Discover, Statuses, Analytics, Juries, Encrypted chat, more...

Destek, sorularınız ve daha fazla bilgi için: https://www.minds.com/help

Açık kaynak kodu için: https://developers.minds.com

İletişim için info@minds.com
